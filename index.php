<?PHP
/*******************************************************************************
 * CGI - Kodtest
 * File: index.php
 * Peter Bergh
 * philemonkey@hotmail.com
 ******************************************************************************/

require_once ("classes/databas.class.php");
require_once ('classes/user.class.php');
session_start();
$title = "CGI - Programmeringsuppgift";


/*******************************************************************************
 * HTML section starts here
 ******************************************************************************/
?>
<!DOCTYPE html>
<html lang="sv-SE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="css/style.css"/>
	<link rel="icon" href="http://studenter.miun.se/~pebe1700/favicon.png" type="image/vnd.microsoft.icon" />
    <script src="js/main.js"></script>
</head>
<body>

<header>

    <h1><?php echo $title ?></h1>
	
</header>

<main>

	<aside>
		<?php include('menu.php'); ?>
	</aside>

	<section class="mainContent">
		Även fast jag helst sitter i Java så valde jag att göra denna uppgift med
		hjälp av PHP och JavaScript. Det föll sig mer naturligt
		utifrån uppgiftsbeskrivningen. Nu sparas alla uppgifter på en databas 
		(som jag har via universitet). Är handen på hjärtat inte värsta liraren då det kommer
		till hur åtkomst till databas fungerar om man skulle köra sidan genom localhost (typ apache); det
		går således bra att kolla koden via repot, men funktionaliteten måste eventuellt testas 
		via den länk jag bifogat.
		<br><br>
		Hantering av visitkort görs från länken "Business Cards" till vänster. 
		Jag fokuserade framförallt på back-end delarna; själva "looken" är ju inte jätterolig. 
		Visitkorten är väl inte vidare "slicka" till sitt utseende :-)
		Den efterfrågade funktionaliteten finns med, om jag uppfattat uppgiften rätt. Ett undantag
		är att jag inte hann implementera uppdatering av bild; bild kan bara läggas till i samband med
		att man skapar ett nytt visitkort.
		<br><br>
		Med vänliga hälsningar<br>
		Peter Bergh
	</section>
	
</main>

<footer>
</footer>

</body>
</html>
