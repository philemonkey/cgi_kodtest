<?php

/*******************************************************************************
 * CGI - Kodtest
 * File: connectionmanager.class.php
 * Peter Bergh
 * philemonkey@hotmail.com
 ******************************************************************************/
require_once('config.class.php');
require_once('connectdb.class.php');

class ConnectionManager
{

	public static function selectQuery($query)
	{
		$conn = ConnectDb::getInstance();
		$result = pg_query($conn->getConnection(), $query);

		if ($result) {
			$arr = pg_fetch_all($result);

			pg_free_result($result);

			return $arr;
		} else {
			return false;
		}
	}

	public static function insertQuery($query, $params)
	{
		$conn = ConnectDb::getInstance();
		$result = pg_query_params($conn->getConnection(), $query, $params);

		if ($result) {
			pg_free_result($result);
			return true;
		} else {
			return false;
		}
	}

	public static function deleteQuery($query)
	{
		$conn = ConnectDb::getInstance();
		$result = pg_query($conn->getConnection(), $query);

		return $result;
	}

	public static function updateQuery($query)
	{
		$conn = ConnectDb::getInstance();
		$result = pg_query($conn->getConnection(), $query);
		return $result;
	}
}
