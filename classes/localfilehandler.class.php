<?php

/*******************************************************************************
 * CGI - Kodtest
 * File: localfilehandler.class.php
 * Peter Bergh
 * philemonkey@hotmail.com
 ******************************************************************************/
require_once('config.php');

class LocalFileHandler
{

	//================================
	//Member functions
	//================================

	public static function isMimeValid($mime)
	{
		$valid = false;
		global $valid_mimes;

		for ($i = 0; $i < count($valid_mimes); $i++) {
			if ($mime === $valid_mimes[$i])
				$valid = true;
		}
		return $valid;
	}

	public static function decodeBase64($arr)
	{
		for ($i = 0; $i < count($arr); $i++) {
			$arr[$i] = base64_decode($arr[$i]);
		}
		return $arr;
	}
}
