<?php

/*******************************************************************************
 * CGI - Kodtest
 * File: user.class.php
 * Peter Bergh
 * philemonkey@hotmail.com
 ******************************************************************************/
require_once('connectionmanager.class.php');
require_once('config.php');
require_once('databas.class.php');

class User implements JsonSerializable
{

	//================================
	//Member variables
	//================================
	private $id;
	private $firstname;
	private $surname;
	private $telephone;
	private $email;
	private $image;
	//================================
	//Member functions
	//================================

	//Constructor
	public function __construct($id, $firstname, $surname, $telephone, $email, $image)
	{

		$this->id = $id;
		$this->firstname = $firstname;
		$this->surname = $surname;
		$this->telephone = $telephone;
		$this->email = $email;
		$this->image = $image;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getFirstname()
	{
		return $this->firstname;
	}

	public function getSurname()
	{
		return $this->surname;
	}

	public function getTelephone()
	{
		return $this->telephone;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function getImage()
	{
		return $this->image;
	}


	public function jsonSerialize()
	{
		return array(
			'id' => $this->id,
			'firstname' => $this->firstname,
			'surname' => $this->surname,
			'telephone' => $this->telephone,
			'email' => $this->email,
			'image' => $this->image
		);
	}
}
