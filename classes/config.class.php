<?php

/*******************************************************************************
 * CGI - Kodtest
 * File: congig.class.php
 * Peter Bergh
 * philemonkey@hotmail.com
 ******************************************************************************/
require_once('config.php');


class Config
{

	public static function getDbDsn()
	{
		global $host;
		global $port;
		global $dbname;
		global $user;
		global $password;

		$dsn = "host=$host port=$port dbname=$dbname user=$user password=$password";
		return $dsn;
	}
}
