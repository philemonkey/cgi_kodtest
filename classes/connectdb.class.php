<?php

/*******************************************************************************
 * CGI - Kodtest
 * File: connectdb.class.php
 * Peter Bergh
 * philemonkey@hotmail.com
 ******************************************************************************/

require_once('config.class.php');

class ConnectDb
{

	private static $instance = null;
	private $conn;

	private function __construct()
	{
		$this->conn = pg_connect(Config::getDbDsn());
	}

	public static function getInstance()
	{
		if (!self::$instance) {
			self::$instance = new ConnectDb();
		}

		return self::$instance;
	}

	public function getConnection()
	{
		return $this->conn;
	}
}
