/*******************************************************************************
 * CGI - Kodtest
 * File: main.js
 * Peter Bergh
 * philemonkey@hotmail.com
 ******************************************************************************/
 
 
 /*******************************************************************************
 * Util functions
 ******************************************************************************/
 
// snabbfunktion för att hitta element från ID.
function byId(id) {
    return document.getElementById(id);
}

// snabbfunktion för skapa nytt element.
function ce(tag) {
	return document.createElement(tag);
}

// snabbfunktion för att hitta element från class.
function byClassname(classname) {
    return document.getElementsByClassName(classname);
}

// Gör om en container till en array.
function toArray(list) {
    return Array.prototype.slice.call(list);
}

/*
Name: 	removeChildren
Brief:	Removes all child-nodes of a parent-node.
Param (parent): The node which will have its child-nodes removed.
*/	
function removeChildren(parent)
{
	while (parent.firstChild)
		parent.removeChild(parent.firstChild);
}

// returnerar all text som kommer efter det sista "/" i en webbsideadress.
function getPageSection (pathname) 
{
	var source = pathname.split("/");
	return source[source.length-1];
}


 
var xhr;
var input;
var images = null;




/*******************************************************************************
/* Function initAdminpage */
/*
	Kickar igång "admin"-sida.
*/
function initAdminpage() {
	
	editButtons = toArray(byClassname("buttonEditUser"));
	editButtons.forEach(function(button) {
		editUser(button, "change");
	});
	
	byId('buttonCreateUser').addEventListener('click', function() {
		if(this.innerHTML === "Create") {
			
			byId("message_to_admin").style.display = "none";
			
			var adminArea = byId("admin_area");
			
			var form = ce("form"); form.className = "editUser";
			var bEdit = ce("button"); bEdit.type = "button"; bEdit.className = "buttonEditUser"; bEdit.innerHTML = "Edit";
			var field = ce("fieldset");field.className = "formFieldset"; field.disabled = true;
			var labelID = ce("label"); labelID.className = "userId"; field.disabled = true;	
			var labelF = ce("label"); labelF.className = "formLabel"; labelF.innerHTML = "first name";

			var inputF = ce("input"); inputF.type = "text"; inputF.name = "firstname"; inputF.className = "inputFirstname";
			var labelS = ce("label"); labelS.className = "formLabel"; labelS.innerHTML = "surname";
			var inputS = ce("input"); inputS.type = "text"; inputS.name = "surname"; inputS.className = "inputSurname";
			var labelT = ce("label"); labelT.className = "formLabel"; labelT.innerHTML = "telephone";
			var inputT = ce("input"); inputT.type = "text"; inputT.name = "telephone"; inputT.className = "inputTelephone";
			var labelE = ce("label"); labelE.className = "formLabel"; labelE.innerHTML = "email";
			var inputE = ce("input"); inputE.type = "text"; inputE.name = "email"; inputE.className = "inputEmail";
			var inputP = ce("input"); inputP.type = "file"; inputP.name = "files[]"; inputP.id = "files"; inputP.className = "inputFile";

		
			field.appendChild(labelID);
			field.appendChild(labelF);
			field.appendChild(inputF);
			field.appendChild(labelS);
			field.appendChild(inputS);
			field.appendChild(labelT);
			field.appendChild(inputT);
			field.appendChild(labelE);
			field.appendChild(inputE);
			field.appendChild(inputP);
			
			form.appendChild(bEdit);
			form.appendChild(field);
			adminArea.appendChild(form);

			inputFiles = byId("files");
			inputFiles.addEventListener('change', browseFiles, false);
			
			editUser(bEdit, "add");
			this.innerHTML = "Undo"; 
		}
		else {
			var form = byClassname("editUser");
			form = form[form.length-1];
			form.parentNode.removeChild(form);
			this.innerHTML = "Create";
		}

	},false);

}


function editUser(button, action) {
	
	var form = button.parentNode;
		
	var fieldset = button.nextElementSibling;
	var id = fieldset.getElementsByClassName('userId')[0].innerHTML;	
	var inputFirstname = fieldset.getElementsByClassName('inputFirstname')[0];
	var firstname = inputFirstname.value;
	var inputSurname = fieldset.getElementsByClassName('inputSurname')[0];
	var surname = inputSurname.value;
	var inputTelephone = fieldset.getElementsByClassName('inputTelephone')[0];
	var telephone = inputTelephone.value;
	var inputEmail = fieldset.getElementsByClassName('inputEmail')[0];
	var email = inputEmail.value;
	var deleteButton = fieldset.getElementsByClassName('buttonDeleteUser')[0];
	


	if(action === "change") {
		deleteButton.addEventListener('click', function() 
		{
			if(confirm("This will erase the business card permanently, proceed?")) 
			{
				console.log("id is: " + id);
				updateUser("delete", id, firstname, null, null, null);
				
				form.parentNode.removeChild(form);
			}
		
		},false);
	}
	
	
	button.addEventListener('click', function() 
	{
		
		if(fieldset.disabled === true) {
			fieldset.disabled = false;
		
			this.innerHTML = "Update";
		}

		else {
			if(inputFirstname.value !== firstname || inputSurname.value !== surname || inputTelephone.value !== telephone || inputEmail.value !== email || images !== null) {
				if(confirm("proceed?")) 
				{
					updateUser(action, id, inputFirstname.value, inputSurname.value, inputTelephone.value, inputEmail.value, images);
					firstname = inputFirstname.value;
					surname = inputSurname.value;
					telephone = inputTelephone.value;
					email = inputEmail.value;
					
				}
				else{
					firstname = inputFirstname.value;
					surname = inputSurname.value;
					telephone = inputTelephone.value;
					email = inputEmail.value;
				}
			
			}
			fieldset.disabled = true;
			this.innerHTML = "Edit";
		}	
	},false);

}

/*******************************************************************************
/* Function updateUser */
/*
	Skickar info om förändring av/tillagd visitkort till servern.
*/
function updateUser(action, id, firstname, surname, telephone, email) {

	
	if(action === "add") {
		if(firstname === "" || surname === "" || telephone === "" || email === "" || images === null) {
			byId("message_to_admin").innerHTML = "all fields must be filled in!";
			byId("message_to_admin").style.display = "block";

		}
		else{
			var userInfo = "action="+action + "&id="+id + "&firstname="+firstname + "&surname=" + surname + "&telephone=" + telephone + "&email=" + email + "&image=" + images;
			
			xhr.open("POST", 'usermanager.php', true);
			xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhr.addEventListener('readystatechange', processUserChange, false);
			xhr.send(userInfo);
			images = null;
		}
		
	}
	else {
	
		var userInfo = "action="+action + "&id="+id + "&firstname="+firstname + "&surname=" + surname + "&telephone=" + telephone + "&email=" + email;
		xhr.open("POST", 'usermanager.php', true);
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhr.addEventListener('readystatechange', processUserChange, false);
		xhr.send(userInfo);
	
	}

}

/*******************************************************************************
/* Function processUserChange */
/*
	Inväntar meddelande från servern kring hur förändringar av visitkortet gick.
*/
function processUserChange() {
	if(xhr.readyState === 4 && xhr.status === 200) 
	{
		xhr.removeEventListener('readystatechange', processUserChange, false);
		var response = JSON.parse(this.responseText);
		var toAdmin = byId("message_to_admin");
		if(response[0] === "add"){
			byId('buttonCreateUser').innerHTML = "Create";
			var form = byClassname("editUser");
			form = form[form.length-1];
			form.parentNode.removeChild(form);
			if(response[1] === true)
				toAdmin.innerHTML = "User was added successfully";
			else
				toAdmin.innerHTML = "Username already exists!";
			
			toAdmin.style.display = "block";
			location.reload();
		}
	}
}




/*******************************************************************************
/*******************************************************************************
/*******************************************************************************
** START OF FILE UPLOAD ********************************************************/


/*******************************************************************************
/* Function browseFiles */
function browseFiles(event) {
	input = event.target;
	var _mimeType = new Array();
	
	for(var i = 0; i < input.files.length; i++)  
	{
		_mimeType[i] = input.files[i].type;
	}
	validateImages(JSON.stringify(_mimeType));
}

/*******************************************************************************
/* Function validateImages */
/*
	Skickar en array med mimes till servern, där server-sidan är
	ansvarig för att granska vilka mimes som är giltiga.
*/
function validateImages(mimeType) {
	
	xhr.open("POST", 'validateimages.php', true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.addEventListener('readystatechange', validateImagesResponse, false);
	xhr.send("mimeType=" + mimeType);		
}


/*******************************************************************************
/* Function validateImagesResponse */
/*
	Tar hand om merparten av klient-sidans hantering av bilder.
	Förbereder bilder för att skickas till servern.
*/
function validateImagesResponse() {
	
	if(xhr.readyState === 4 && xhr.status === 200) 
	{
		xhr.removeEventListener('readystatechange', validateImagesResponse, false);
		/*	response innehåller två arrayer; en array med index för alla giltiga filer
			och en array med index för alla ogiltiga filer. */
		var response = JSON.parse(this.responseText); 
								
		var reader = [response[0].length];	// en array med samma storlek som antalet giltiga bild-filer.
		var countdown = response[0].length;	// en nedräknare för att veta när alla bilder har behandlats.
		var _images = new Array();			// array för att hålla samtliga bilders data.
		var cnt = 0;						// en räknare för index.
		
		for(var i = 0; i < response[0].length; i++) 
		{
			/* varje reader-element instansierar en FileReader och läser 
			varje giltig bilds data samt filnamn. */
			reader[i] = new FileReader();	
			reader[i].readAsDataURL(input.files[response[0][i]]);
			reader[i].onloadend = function(){
				
			
				var img = new Image; // Skapar ett Image-objekt.
				img.onload = function() 
				{
					/* skickar image-objektet, innehållandes resultatet 
					från FileReadern, och filnamnet till funktionen "scale" */
					scale(img); 
				}
				img.src = this.result;
				
				
			}
		}
		
		/*****************************************
		/* function scale */
		async function scale(img) {
			let getData = new Promise((resolve, reject) => {resolve(resize(img))})

			let result = await getData 	// väntar på getData (som skalar ner bilden). 
			var blob = blobify(result);	// Returnerar en blob av Image-objektet
			toBase64(blob);	// Den nerskalade bilden är nu en blob och kan "encodas" till Base64.
		}
		
		/*****************************************
		/* function resize */
		function resize(img) {
			var temp = img.width;
			img.width = 200;
			//img.height = img.height/(temp/200);
			img.height = 200;
			return img;
		}
		
		/*****************************************
		/* function blobify */
		/*
			"Ritar" ett Image-objekt till canvas för
			att kunna konvertera informationen till en "blob".
		*/
		function blobify(img) {
			
			var canvas = document.createElement("canvas");
			canvas.width = img.width;
			canvas.height = img.height;
			var ctx = canvas.getContext("2d");
			ctx.drawImage(img, 0, 0, img.width, img.height);
			var dataURL = canvas.toDataURL("image/jpeg");
			return dataURL;
		}
		
		/*****************************************
		/* function toBase64 */
		/*
			"Encodar" data till Base64.
		*/
		async function toBase64(data)
		{
			let getData = new Promise((resolve, reject) => {resolve(btoa(data))})
			let result = await getData 		// väntar på getData (som kodar om bilden). 
			toImageArray(result);	// Den omkodade bilden är klar för att skickas till servern.
		}
		
		/*****************************************
		/* function toImageArray */
		function toImageArray(arg) 
		{
			_images[cnt] = arg;
			cnt++;
			countdown--;
			/* 	När countdown är 0 så har alla bilder behandlats
				och arrayen som håller samtliga data för bildernas
				kan postas till servern */
			if(countdown === 0)
			{
				images = JSON.stringify(_images);
				
				//postImagesToServer(images, filenames); 
			}
		}
	}		
}




/******************************************************END OF FILE UPLOAD*******
********************************************************************************
*******************************************************************************/


 



	

function main() {

	try {
        if (window.XMLHttpRequest) {
            // för IE7+, Firefox, Chrome, Opera, Safari
            xhr = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            // för IE6, IE5
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else {
            throw new Error('Cannot create XMLHttpRequest object');
        }

    } catch (e) {
        alert('"XMLHttpRequest failed!' + e.message);
    }

	var path = getPageSection(window.location.pathname);


	if(path === "admin.php") {
		initAdminpage();
	}
	
	
}	
		
		
		
window.addEventListener("load", main, false);

