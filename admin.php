<?php

/*******************************************************************************
 * CGI - Kodtest
 * File: admin.php
 * Peter Bergh
 * philemonkey@hotmail.com
 ******************************************************************************/
require_once('classes/user.class.php');
require_once('classes/databas.class.php');

session_start();

$title = "Business Cards";
$allUsersRaw = Databas::getAllUsers();
$allUsers = array();
for ($i = 0; $i < count($allUsersRaw); $i++) {
	$allUsers[] = new User($allUsersRaw[$i]['id'], $allUsersRaw[$i]['name'], $allUsersRaw[$i]['surname'], $allUsersRaw[$i]['telephone'], $allUsersRaw[$i]['email'], $allUsersRaw[$i]['image']);
}


/*******************************************************************************
 * HTML section starts here
 ******************************************************************************/
?>
<!DOCTYPE html>
<html lang="sv-SE">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $title ?></title>
	<link rel="stylesheet" href="css/style.css" />
	<link rel="icon" href="http://studenter.miun.se/~pebe1700/favicon.png" type="image/vnd.microsoft.icon" />
	<script src="js/main.js"></script>
</head>

<body>

	<header>
		<h1><?php echo $title ?></h1>
	</header>

	<main>

		<aside>
			<?php include('menu.php'); ?>
		</aside>

		<section class="mainContent">
			<section id="admin_area">
				<?php for ($i = 0; $i < count($allUsers); $i++) { ?>


					<form class="editUser">
						<button type="button" class="buttonEditUser">Edit</button>
						<fieldset class="formFieldset" disabled>
							<label class="formLabel">first name:</label>
							<input type="text" name="firstname" class="inputFirstname" value="<?php echo $allUsers[$i]->getFirstname() ?>" />
							<label class="userId"><?php echo $allUsers[$i]->getId() ?></label>
							<br>
							<label class="formLabel">surname:</label>
							<input type="text" name="surname" class="inputSurname" value="<?php echo $allUsers[$i]->getSurname() ?>" /><br>
							<label class="formLabel">telephone:</label>
							<input type="text" name="telephone" class="inputTelephone" value="<?php echo $allUsers[$i]->getTelephone() ?>" /><br>
							<label class="formLabel">email:</label>
							<input type="text" name="email" class="inputEmail" value="<?php echo $allUsers[$i]->getEmail() ?>" /><br>
							<?php $img = base64_decode($allUsers[$i]->getImage()); ?>
							<?php echo "<img src=\"" . $img . "\">"; ?><br>
							<button type="button" class="buttonDeleteUser">Delete</button>
						</fieldset>
					</form>
				<?php }  ?>

				<button type="button" id="buttonCreateUser">Create</button>
				<p id="message_to_admin" style="display: none;"></p>
			</section>
		</section>
		<section class="emptySpace"></section>

	</main>

	<footer>
	</footer>

</body>

</html>