<?php
/*******************************************************************************
 * CGI - Kodtest
 * File: validateimages.php
 * Peter Bergh
 * philemonkey@hotmail.com
 ******************************************************************************/
require_once ('classes/localfilehandler.class.php');

if ($_POST['mimeType']) 
{
	$arr = json_decode($_POST['mimeType']);
	$valid_files = array();
	$invalid_files = array();

	for($i = 0 ; $i < count($arr) ; $i++) {
		if (LocalFileHandler::isMimeValid($arr[$i])) {
			$valid_files[] = $i;		
		}
		else {
			$invalid_files[] = $i;	
		}
	}
	
	$send_back = array($valid_files, $invalid_files);
	
	header('Content-Type: application/json');
	echo json_encode($send_back);
	
}
