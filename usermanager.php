<?PHP

/*******************************************************************************
 * CGI - Kodtest
 * File: usermanager.php
 * Peter Bergh
 * philemonkey@hotmail.com
 ******************************************************************************/
require_once("classes/databas.class.php");
require_once("classes/user.class.php");
require_once("config.php");

session_start();

if (!empty($_POST)) {
	if ($_POST['action'] === "delete") {
		Databas::deleteUser($_POST['id']);
	} else if ($_POST['action'] === "change") {
		Databas::updateUser($_POST['id'], $_POST['firstname'], $_POST['surname'], $_POST['telephone'], $_POST['email']);

	} else if ($_POST['action'] === "add") {
		$image = json_decode($_POST['image']);		
		$result = Databas::addUser($_POST['firstname'], $_POST['surname'], $_POST['telephone'], $_POST['email'], $image[0]);
	}
	$arr[] = $_POST['action'];
	$arr[] = $result;
	header('Content-Type: application/json');
	echo json_encode($arr);
}
